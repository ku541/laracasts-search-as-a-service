<?php

namespace App\Contracts;

/**
 * Algolia Wrapper
 */
interface Search
{
    public function index($index);

    public function get($query);
}
