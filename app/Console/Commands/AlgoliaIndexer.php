<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Contracts\Search;

class AlgoliaIndexer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laracasts:index {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Search $search)
    {
        $table = $this->argument('table');

        $collection = DB::table($table)->get()->map(function ($model) {
            $model->objectID = $model->id;

            return (array) $model;
        });

        $search->index($table)->saveObjects($collection);

        $this->info('All Done!');
    }
}
