<?php

use App\Contracts\Search;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Search $search) {
    $results = $search->index('getstarted_actors')
                      ->get(request('name'));

    return view('welcome', compact('results'));
});
