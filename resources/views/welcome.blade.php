<!doctype html>
<html>
    <head>
        <title>Laravel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <h1>Search Results</h1>
            <ul class="list-group">
                @foreach ($results as $actor)
                    <li class="list-group-item">{{ $actor['name'] }}</li>
                @endforeach
            </ul>
        </div>
    </body>
</html>
